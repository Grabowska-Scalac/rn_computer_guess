import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';

import Header from './components/Header';
import StartGamePage from './pages/StartGamePage';
import GamePage from './pages/GamePage';
import GameOverPage from './pages/GameOverPage';

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
  });
};

export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [guessRounds, setGuessRounds] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);

  if (!isLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setIsLoaded(true)}
        onError={(err) => console.log(err)}
      />
    );
  };

  const startNewGameHandler = () => {
    setGuessRounds(0);
    setUserNumber(null);
  }

  const startGameHandler = selectedNumber => {
    setUserNumber(selectedNumber);
  };

  const gameOverHandler = nrOfRounds => {
    setGuessRounds(nrOfRounds);
  }

  let content = <StartGamePage onStartGame={startGameHandler} />

  if (userNumber && guessRounds <= 0) {
    content = (<GamePage userChoice={userNumber} onGameOver={gameOverHandler} />);
  } else if (guessRounds > 0) {
    content = (<GameOverPage
      roundsNumber={guessRounds}
      userNumber={userNumber}
      onReset={startNewGameHandler}
    />);
  }

  return (
    <View style={styles.screen}>
      <Header text="Guess a Number" />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1
  },
});

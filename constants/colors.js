export default {
  primary: '#36477d',
  secondary: '#485b96',
  accent: '#f87082',
  secondAccent: '#7c3660'
}
import React from 'react';
import { View, Text, StyleSheet, Button, Alert, Image } from 'react-native';
import Colors from '../constants/colors';
import Fonts from '../constants/fonts';


const GameOver = props => {
  return (
    <View style={styles.pageWrapper}>
      <Text style={{ ...Fonts.title, ...styles.text }}>You Win!</Text>
      <View style={styles.imageContainer}>
        <Image
          // source={require('../assets/success.png')} // for local imgs you don't need width and height
          source={{uri: 'https://cdn.pixabay.com/photo/2018/04/23/09/59/hands-3343735_960_720.jpg'}}
          style={styles.image}
          // diffrent asspect ratio modes
          resizeMode='cover'
        />
      </View>
      <Text style={{ ...Fonts.bodyText, ...styles.text }}>Number of rounds: {props.roundsNumber}</Text>
      <Text style={{ ...Fonts.bodyText, ...styles.text }}>Number was: {props.userNumber}</Text>
      <View style={styles.buttonWrapper}>
        <Button color={Colors.accent} title="NEW GAME" onPress={props.onReset} />
      </View>
    </View>
  )


}

const styles = StyleSheet.create({
  pageWrapper: {
    flex: 1,
    alignItems: 'center',
    paddingTop:20,
    backgroundColor: Colors.secondary,

  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150, // for circle choose half width&height
    borderColor: 'black',
    borderWidth: 3,
    overflow: 'hidden',
    marginVertical: 20
  },
  image: {
    width: '100%',
    height: '100%',

  },
  text: {
    fontSize: 18,
    color: 'white'
  },
  buttonWrapper: {
    padding: 20,
  }
});

export default GameOver;
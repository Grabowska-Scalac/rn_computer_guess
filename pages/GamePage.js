import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import Number from '../components/Number';
import Card from '../components/Card';
import MainButton from '../components/MainButton';

import Colors from '../constants/colors';

const generateRandomNr = (min, max, exclude) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  let randomNr = Math.floor((Math.random() * (max - min) + min));
  randomNr = (randomNr === exclude)
    ? generateRandomNr(min, max, exclude)
    : randomNr;
  return randomNr;
}


const GamePage = props => {

  const [currentGuess, setCurrentGuess] = useState(generateRandomNr(1, 100, props.userChoice))
  const [rounds, setRounds] = useState(0);
  const currentMin = useRef(1);
  const currentMax = useRef(100);

  const { userChoice, onGameOver } = props;

  useEffect(() => {
    if (currentGuess === props.userChoice) {
      onGameOver(rounds);
    }
  }, [currentGuess, userChoice, onGameOver]);

  const nextChoiceHandler = direction => {
    const isUserNotHonest = (direction === 'lower' && currentGuess < props.userChoice) || (direction === 'greater' && currentGuess > props.userChoice);
    if (isUserNotHonest) {
      Alert.alert(
        'Wrong choice',
        `Your nr was ${direction}`,
        [{ text: 'Try again!', style: 'cancel' }]
      );
      return;
    }
    if (direction === 'lower') {
      currentMax.current = currentGuess - 1;
    } else {
      currentMin.current = currentGuess + 1;
    }
    const nextNr = generateRandomNr(currentMin.current, currentMax.current, currentGuess);
    setCurrentGuess(nextNr);
    setRounds(curRounds => curRounds + 1)
  }

  return (
    <View style={styles.pageWrapper}>
      <Text style={{ ...styles.text, ...props.style }}>Computer's Guess</Text>
      <Card style={styles.numberContainer}>
        <Number text={currentGuess} />
      </Card>
      <View style={styles.buttonsRow}>
        <MainButton style={styles.button} color={Colors.accent} onPress={nextChoiceHandler.bind(this, 'lower')} >
          <Ionicons name="md-remove" size={24} color="white" />
        </MainButton>
        <MainButton style={styles.button} color={Colors.accent} onPress={nextChoiceHandler.bind(this, 'greater')} >
          <Ionicons name="md-add" size={24} color="white" />
        </MainButton>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  pageWrapper: {
    alignItems: 'center',
    padding: 30,
    backgroundColor: Colors.secondary,
    flex: 1
  },
  text: {
    fontSize: 22,

  },
  numberContainer: {
    margin: 20,
    backgroundColor: 'white',
  },
  buttonsRow: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30

  }
})


export default GamePage;
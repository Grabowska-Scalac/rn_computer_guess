import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';

import Card from '../components/Card';
import Input from '../components/Input';
import Number from '../components/Number';
import MainButton from '../components/MainButton';

import Colors from '../constants/colors';
import Fonts from '../constants/fonts';

const StartGamePage = props => {
  const [enteredValue, setEnteredValue] = useState('');
  const [confirmed, setConfirmed] = useState(false);
  const [selectedNr, setSelectedNr] = useState(0)

  const numberValueHandler = inputText => {
    setEnteredValue(inputText.replace(/[^0-9]/g, ''));
  }

  const resetValueHandler = () => {
    setEnteredValue('');
    setConfirmed(false);
  };

  const confirmValueHandler = () => {
    const chosenNr = parseInt(enteredValue);
    if (isNaN(chosenNr) || chosenNr <= 0 || chosenNr > 99) {
      Alert.alert(
        "Invalid Number.",
        "The number has to be between 1 and 99.",
        [{ text: 'Okay', style: 'destructive', onPress: resetValueHandler }]
      );
      setConfirmed(false);
      return;
    }
    setConfirmed(true);
    setEnteredValue('');
    // even if we reset, it will happen in new rerender, so for now entered Value is still a number
    setSelectedNr(chosenNr);
    Keyboard.dismiss();

  };
  let confirmedOutput;

  if (confirmed) {
    confirmedOutput =
      <Card style={styles.summaryWrapper}>
        <Text> Chosen Nr is </Text>
        <Number text={selectedNr} />
        <MainButton onPress={() => { props.onStartGame(selectedNr) }}>
          START GAME
        </MainButton>
      </Card>
  }

  return (
    // For hidding the keyboard
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.pageWrapper}>
        <Text style={{ ...Fonts.title, ...styles.title }}> Start New Game! </Text>
        <Card style={styles.inputWrapper}>
          <Text style={styles.inputTitle}>Select a Number</Text>
          <Input style={styles.input}
            blurOnSubmit
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="number-pad" //  numeric works only on ISO
            maxLength={2}
            onChangeText={numberValueHandler}
            value={enteredValue}
          />
          <View style={styles.buttonsRow}>
            <View>
              <Button
                // style={styles.buttonReset}
                color={Colors.secondAccent}
                title="Reset"
                onPress={resetValueHandler}
              />
            </View>
            <Button
              // style={styles.buttonConfirm}
              color={Colors.primary}
              title="Confirm"
              onPress={confirmValueHandler}
            />
          </View>
        </Card>
        {confirmedOutput}
      </View>

    </TouchableWithoutFeedback>

  );
};

const styles = StyleSheet.create({
  pageWrapper: {
    flex: 1,
    padding: 15,
    alignItems: 'center',
    backgroundColor: Colors.secondary,
  },
  title: {
    fontSize: 25,
    marginVertical: 10,
    marginBottom: 20,
    color: 'white',
    // fontFamily: 'open-sans-bold'
    // font Family need to be added on Text component, is not passed
  },
  inputWrapper: {
    width: 300,
    maxWidth: '80%',
  },
  inputTitle: {
    color: 'white',
    fontSize: 18,
  },
  input: {
    width: 50,
    textAlign: 'center'
  },
  buttonsRow: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  summaryWrapper: {
    width: 300,
    maxWidth: '80%',
    marginTop: 15,
    backgroundColor: 'white',
    alignItems: 'center'
  }

});
export default StartGamePage;
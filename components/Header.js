import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = props => {
  return (
    <View style={styles.titleWrapper}>
      <Text style={styles.title}>{props.text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  titleWrapper: {
    width: '100%',
    height: 90,
paddingTop: 36,
backgroundColor: '#36477d',
alignItems: 'center',
justifyContent: 'center'
  },
  title: {
    color: '#c16b84',
    fontSize: 18,

  }
})


export default Header;
import React from 'react';
import {View, StyleSheet} from 'react-native';
import Colors from '../constants/colors'

const Card = props => {
  return(
    <View style={{...styles.cardWrapper, ...props.style}}>{props.children}</View>
  )
}

const styles = StyleSheet.create({
  cardWrapper: {
    alignItems: 'center',
    padding: 20,
    backgroundColor: Colors.accent,
    borderColor: Colors.primary,
    borderRadius: 20,
    borderWidth: 3,
    //work only on IOS
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.56,

    // work only on Android - look lame
    elevation: 5

  }
});

export default Card;
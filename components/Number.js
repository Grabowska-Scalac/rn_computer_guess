import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Colors from '../constants/colors';

const Number = props => {
  return (
    <View style={styles.textWrapper}>
      <Text style={{ ...styles.text, ...props.style }}>{props.text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  textWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 30
  },
  text: {
    fontSize: 56,

  }
})


export default Number;